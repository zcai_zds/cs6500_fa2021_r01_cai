# **Zhaoyuan Cai**
### 2212 Master of Science in Analytics
***
**1.What did you learn from Chapter 01: Big Data Analtyics at a 10,000 Foot View? What do you think are the most important takeaways from this provided chapter?**
- answer: I learned about two big data analysis software, Hadoop and Spark. I think it's the first time I have come into contact with Hadoop,   and Spark, I have learned Sql and have some experience. I also learned the difference between data science and data analysis. The most important takeaways from this provided chapter is data science projects are iterative in nature, but data analysis projects are not interactive.
***
**2.What is your technical background? Are you from Computer Science? Statistics? Do you have programming experience? Are you strong in statistical analysis? Have you used technologies like Hadoop, Spark, etc. before?**
- answer:My major is statistics rather than computer science. I have some SQL and R programming experience, and I have taught myself some basic Python syntax. I have never been exposed to Hadoop.
***
**3.What most concerns you about this course? Asked another way, "What do you think you'll struggle with in this course?"**
- I think any computer course will make me feel confused and struggled, because in my impression, computer science is a rare course, especially the logic of programming sentences. Some complicated grammar will make me very confused. In addition, there are many types of programming software, which will also challenge my brain.
***
**4.What do you expect to get out of this course? As this course delves into Big Data analytics, I would like to know what theoretical and practical knowledge you would like gain (or that you think will be provided) by this course？**
- I hope that after I finish this course, I can fully master Python and Hadoop, because I have delivered data analysis positions for many companies, and most companies require both Python and Hadoop skills. I am eager to learn these things to plan my life.
***
**5.Are there any aspects of typical online courses that you think should not be included in this course?**
- answer：No, I think the content of this course is very necessary.
***
**6.Are there things that will help you succeed in this online course?**
- answer: I think comparing with others and my goals will make me successful in this online course. Because I am eager to learn just to apply it to work.
***
**7.Is there anything else you would like the instructor to know?**
- answer: I am currently in the internship period, and I am now in China, so there will be a time difference with the United States, so I may encounter a problem that is difficult to solve and may send you an email inquiry during your break. Please also pay attention to students’ emails frequently. thank you very much!
